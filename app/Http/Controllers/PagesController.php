<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{

    public function welcome() {
      $message = "HELLO!!!";
      $name = "Guillermo";
        return view('welcome', compact('message','name'));
    }
    public function about() {
        return view('about');
    }
    public function services() {
      $services = ['Android Development','Game Development','Networking'];
        return view('services', compact('services'));
    }
    public function contact() {
        return view('contact');
    }
}
