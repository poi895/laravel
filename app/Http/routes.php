<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PagesController@welcome');
//about
Route::get('/about', 'PagesController@about');
//services
Route::get('/services', 'PagesController@services');
//contact
Route::get('/contact', 'PagesController@contact');
