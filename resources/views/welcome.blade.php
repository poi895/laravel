@extends('Layouts.master')
@section('content')
<div class="row">
      <div class="col-lg-12 text-center">
        <div class = "jumbotron">
            <h1>{{$message}}</h1>
              <p class="lead"></p>
              <ul class="list-unstyled">
                  <li>{{$name}}</li>
                  <li></li>
              </ul>
          </div>
      </div>
  </div>
        <!-- /.row -->
@endsection
