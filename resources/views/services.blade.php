@extends('Layouts.master')
@section('content')
<div class="row">
      <div class="col-lg-12 text-center">
            <h1>Services</h1>
              <p class="lead"></p>
              <ul class="list-unstyled">

                @foreach($services as $service)
                <p>{{$service}}</p>
                @endforeach
                
              </ul>
      </div>
  </div>
        <!-- /.row -->
@endsection
